#ifndef INDEXTSV_TSV_INDEX_H
#define INDEXTSV_TSV_INDEX_H

#include <stdint.h>

#define DATA_START 12 + sizeof(uint32_t)
#define HEADER "VOILATSV"

enum TsvType
{
    TSVTYPE_PSI = 0,
    TSVTYPE_DELTAPSI
};

static enum TsvType gTsvType;

struct PsiTsvRow
{
    char * geneName;
    char * geneId;
    char * lsvId;
    char * ePsi;
    char * vPsi;
    char * lsvType;
    char * a5ss;
    char * a3ss;
    char * es;
    char * nJunc;
    char * nExons;
    char * deNovoJns;
    char * chr;
    char * strand;
    char * jnCoords;
    char * exonCoords;
    char * irCoords;
};

struct DeltaPsiTsvRow
{
    char * geneName;
    char * geneId;
    char * lsvId;
    char * eDPsi;
    char * pDPsi;
    char * vDPsi;
    char * ePsi1;
    char * ePsi2;
    char * lsvType;
    char * a5ss;
    char * a3ss;
    char * es;
    char * nJunc;
    char * nExons;
    char * deNovoJns;
    char * chr;
    char * strand;
    char * jnCoords;
    char * exonCoords;
    char * irCoords;
};

union VoilaTsvRow
{
    struct PsiTsvRow psi;
    struct DeltaPsiTsvRow deltapsi;
    char *asArray[0];
};

#endif //INDEXTSV_TSV_INDEX_H
