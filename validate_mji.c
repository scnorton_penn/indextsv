#include <stdio.h>
#include <string.h>
#include "tsv_index.h"

int main(int argc, char * argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s FILE (psi|deltapsi)\n", argv[0]);
        return 1;
    }

    if (strcmp(argv[2], "psi") == 0)
        gTsvType = TSVTYPE_PSI;
    else if (strcmp(argv[2], "deltapsi") == 0)
        gTsvType = TSVTYPE_DELTAPSI;
    else {
        fprintf(stderr, "%s: Unsupported TSV type \"%s\"\n", argv[0], argv[2]);
        return 1;
    }

    FILE * fp = fopen(argv[1], "rb");
    if (fp == NULL) {
        fprintf(stderr, "%s: Unable to open \"%s\" for reading\n", argv[0], argv[1]);
        return 1;
    }
    uint64_t error_flags = 0;

    fseek(fp, 0, SEEK_END);
    if (ftell(fp) < 16) {
        fprintf(stderr, "%s: Length of file \"%s\' too small\n", argv[0], argv[1]);
        return 1;
    }

    if (ftell(fp) % 16) {
        fprintf(stderr, "%s: Length of file \"%s\' not aligned\n", argv[0], argv[1]);
        return 1;
    }

    fseek(fp, 0, SEEK_SET);
    char sentinel[11];
    char trueSentinel[11] = HEADER;
    uint32_t checksum;
    fread(sentinel, sizeof(char), 11, fp);
    if (memcmp(sentinel, trueSentinel, 11) != 0) {
        fprintf(stderr, "%s: The file signature for \"%s\" did not match\n", argv[0], argv[1]);
        return 1;
    }

    enum TsvType tsvType = fgetc(fp);
    if (tsvType != gTsvType)
        error_flags |= 1;

    fread(&checksum, sizeof(uint32_t), 1, fp);

    uint64_t checksumBuffer = 0;
    uint32_t word;
    while (1) {
        fread(&word, sizeof(uint32_t), 1, fp);
        if (feof(fp))
            break;
        checksumBuffer += word;
    }
    uint32_t checksum_xor = checksumBuffer ^ (checksumBuffer >> 32);
    if (checksum_xor != checksum)
        error_flags |= 2;

    fclose(fp);

    if (error_flags & 1)
        fputs("The indexed TSV is not the specified type\n", stderr);
    if (error_flags & 2)
        fputs("The checksum did not match\n", stderr);
    return error_flags != 0;
}
