CC := gcc
CFLAGS := -O3 -std=c99 -D_POSIX_C_SOURCE=200809L

PROGRAMS = indextsv validate_mji

all: $(PROGRAMS)

$(PROGRAMS): %: %.c
	$(CC) $(CFLAGS) -o $@ $<

clean:
	rm -f $(PROGRAMS) $(PROGRAMS:%=%.exe)
