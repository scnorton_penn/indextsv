#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <ctype.h>
#include "tsv_index.h"

static inline void tokenizeTsvRow(char * line, char ** output) {
    char * saveptr;
    while ((*output++ = strtok_r(line, "\t", &saveptr)) != NULL)
        line = NULL;
}

int index_tsv(const char * fname) {
    union VoilaTsvRow curRow;

    FILE * fp = fopen(fname, "r");
    if (fp == NULL) {
        fprintf(stderr, "Error: failed to open \"%s\" for reading\n", fname);
        return 1;
    }

    char *outfname = malloc(BUFSIZ + 1);
    if (outfname == NULL) {
        fclose(fp);
        fputs("Error: failed to reserve output filename\n", stderr);
        return 1;
    }

    sprintf(outfname, "%s.mji", fname);

    FILE * ofp = fopen(outfname, "w+b");
    if (ofp == NULL) {
        fclose(fp);
        free(outfname);
        fprintf(stderr, "Error: failed to open \"%s\" for writing\n", outfname);
        return 1;
    }

    char sentinel[11] = HEADER;
    fwrite(sentinel, sizeof(char), 11, ofp);
    fputc(gTsvType, ofp);
    fseek(ofp, DATA_START, SEEK_SET);

    char * line = NULL;
    size_t lineSize = 0;
    char * indexKey = malloc(BUFSIZ + 1);
    uint32_t lastPos = 0;

    while (getline(&line, &lineSize, fp) > -1) {
        while (isspace(*line)) line++;
        *strrchr(line, '\n') = 0;
        if (line[0] == '#') {
            gTsvType = strstr(line, "dPSI") == NULL ? TSVTYPE_PSI : TSVTYPE_DELTAPSI;
        } else {
            tokenizeTsvRow(line, curRow.asArray);
            if (gTsvType == TSVTYPE_PSI)
                sprintf(indexKey, "%s\t%s\t%s", curRow.psi.chr, curRow.psi.lsvId, curRow.psi.geneName);
            else
                sprintf(indexKey, "%s\t%s\t%s", curRow.deltapsi.chr, curRow.deltapsi.lsvId, curRow.deltapsi.geneName);
            uint32_t keyLen = strlen(indexKey);
            fwrite(&lastPos, sizeof(uint32_t), 1, ofp);
            fwrite(&keyLen, sizeof(uint32_t), 1, ofp);
            fwrite(indexKey, sizeof(char), keyLen, ofp);
        }
        lastPos = ftell(fp);
    }

    fputc(255, ofp);
    fputc(255, ofp);
    fputc(255, ofp);
    fputc(255, ofp);

    while (ftell(ofp) & 15) {
        fputc(0, ofp);
    }
    
    fseek(ofp, DATA_START, SEEK_SET);
    uint64_t checksum = 0;
    uint32_t curWord = 0;
    while (1) {
        fread(&curWord, sizeof(uint32_t), 1, ofp);
        if (feof(ofp))
            break;
        checksum += curWord;
    }
    fseek(ofp, 12, SEEK_SET);
    curWord = checksum ^ (checksum >> 32);
    fwrite(&curWord, sizeof(uint32_t), 1, ofp);

    free(line);
    free(outfname);
    free(indexKey);
    fclose(fp);
    fclose(ofp);
    return errno == ENOMEM ? 1 : 0;
}

int main(int argc, char * argv[]) {
    // USAGE: indextsv filename filename ...
    if (argc < 2) {
        fprintf(stderr, "Usage: %s FILE [FILE ...]\n", argv[0]);
        return 1;
    }

    int errorState = 0;

    for (int i = 1; i < argc; i++) {
        errorState |= index_tsv(argv[i]);
    }

    return errorState;
}
